<!ENTITY torsettings.dialog.title "Sieťové Nastavenia Tor">
<!ENTITY torsettings.wizard.title.default "Pripojiť k Tor">
<!ENTITY torsettings.wizard.title.configure "Sieťové Nastavenia Tor">
<!ENTITY torsettings.wizard.title.connecting "Vytváram spojenie">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Jazyk Prehliadača Tor">
<!ENTITY torlauncher.localePicker.prompt "Prosím vyberte si jazyk">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Kliktnite &quot;Pripojiť&quot; pre pripojenie do siete Tor.">
<!ENTITY torSettings.configurePrompt "Kliknite &quot;Konfigurácia&quot; pre nastavenie siete ak ste v krajite ktorá cenzúruje Tor (ako Egypt, Čína, Turecko) alebo ak sa pripájate zo súkromnej siete ktorá vyžaduje proxy.">
<!ENTITY torSettings.configure "Konfigurácia">
<!ENTITY torSettings.connect "Pripojiť">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Čakám kým sa Tor spustí...">
<!ENTITY torsettings.restartTor "Reštartujte Tor">
<!ENTITY torsettings.reconfigTor "Rekonfigurovať">

<!ENTITY torsettings.discardSettings.prompt "Nakonfigurovali ste Tor premostenia alebo ste uviedli miestne nastavenia proxy.&#160; Pre vytvorenie priameho spojenia so sieťou Tor, musia byť tieto nastavenia odstránené.">
<!ENTITY torsettings.discardSettings.proceed "Odstrániť nastavenia a spojenia">

<!ENTITY torsettings.optional "Voliteľné">

<!ENTITY torsettings.useProxy.checkbox "Na pripojenie k internetu používam proxy">
<!ENTITY torsettings.useProxy.type "Typ Proxy">
<!ENTITY torsettings.useProxy.type.placeholder "vybrať typ proxy">
<!ENTITY torsettings.useProxy.address "Adresa">
<!ENTITY torsettings.useProxy.address.placeholder "IP adresa alebo názov hostiteľa">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Používateľské meno">
<!ENTITY torsettings.useProxy.password "Heslo">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Tento počítač ide cez firewall, ktorý povoľuje iba niektoré porty">
<!ENTITY torsettings.firewall.allowedPorts "Povolené porty">
<!ENTITY torsettings.useBridges.checkbox "Tor je v mojej krajine cenzurovaný">
<!ENTITY torsettings.useBridges.default "Zvoľte zabudované premostenie">
<!ENTITY torsettings.useBridges.default.placeholder "zvoľte premostenie">
<!ENTITY torsettings.useBridges.bridgeDB "Vyžiadať premostenie od torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Zadajte znaky z obrázka...">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Vyžiadať nový obrátok na rozpoznanie.">
<!ENTITY torsettings.useBridges.captchaSubmit "Odoslať">
<!ENTITY torsettings.useBridges.custom "Poskytnúť mauálne známe premostenie.">
<!ENTITY torsettings.useBridges.label "Zadať premostenie z dôveryhodného zdroja.">
<!ENTITY torsettings.useBridges.placeholder "napíšte adresu:port (jedna na riadok)">

<!ENTITY torsettings.copyLog "Skopírovať záznam Tor do schránky clipboard">

<!ENTITY torsettings.proxyHelpTitle "Pomoc pre Proxy">
<!ENTITY torsettings.proxyHelp1 "Lokálne proxy môže byť vyžadované ak je projojenie cez pracovnú, školskú alebo univerzitnú sieť.&#160;Ak si nie ste isný či je proxy vyžadované, pozrite na nastavenie Internetového pripojenia v inom internetovom prehliadači alebo v nastaveniach siete systému.">

<!ENTITY torsettings.bridgeHelpTitle "Nápoveda o relé premosteniach">
<!ENTITY torsettings.bridgeHelp1 "Premostenia sú nezaznamenané relé, ktoré komplikujú blokovanie pripojení do siete Tor.&#160; Každý typ premostenia používa inú metódu aby sa vyhlo cenzúre.&#160; Obfs kamuflujú vašu komunikáciu ako náhodný šum a ostatné sa tvária že pristupujú na iné služby ako Tor. ">
<!ENTITY torsettings.bridgeHelp2 "Vzhľadom na to ako niektoré krajiny blokujú Tor, niektoré premostenia fungujú v niektorých krajinách ale nie v iných.&#160; Ak si nie ste istý ktoré premostenia fungujú u vás, navštívte torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Prosím počkajte na vytvorenie pripojenia do siete Tor.&#160; Môže to trvať niekoľko minút. ">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Pripojenie">
<!ENTITY torPreferences.torSettings "Nastavenia Tor">
<!ENTITY torPreferences.torSettingsDescription "Prehliadač Tor smeruje váš prenos cez sieť Tor, prevádzkovanú tisíckami dobrovoľníkov z celého sveta." >
<!ENTITY torPreferences.learnMore "Zisti viac">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Pripojený">
<!ENTITY torPreferences.statusInternetOffline "Odpojený">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Pripojené">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Zisti viac">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Mosty">
<!ENTITY torPreferences.bridgesDescription "Premostenia vám pomáhajú pripojiť sa k sieti Tor tam, kde je Tor blokovaný. V závislosti od toho, kde sa nachádzate niektoré premostenie môže fungovať lepšie ako iné.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatic">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Odstrániť">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Skopírované!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Požiadať o premostenie...">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Pokročilé">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Zobraziť záznamy...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Zrušiť">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Vyžiadať Premostenie">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Spájam sa s databázou Premostení. Prosím čakajte.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Vyrieš CAPTCHA na vyžiadanie premostenia.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Riešenie nie je správne. Prosím skúste znova.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Záznamy Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "Pripája sa…">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automaticky">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Skúsiť znova">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
