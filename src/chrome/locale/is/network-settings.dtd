<!ENTITY torsettings.dialog.title "Netkerfisstillingar Tor">
<!ENTITY torsettings.wizard.title.default "Tengjast við Tor-netið">
<!ENTITY torsettings.wizard.title.configure "Netkerfisstillingar Tor">
<!ENTITY torsettings.wizard.title.connecting "Kem á tengingu">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tungumál Tor-vafra">
<!ENTITY torlauncher.localePicker.prompt "Veldu tungumál">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Smelltu á &quot;Tengjast&quot; til að tengjast við Tor.">
<!ENTITY torSettings.configurePrompt "Smelltu á “Stilla” til að breyta netstillingum ef þú ert í landi sem ritskoðar umferð um Tor (svo sem Egyptalandi, Kína, Tyrklandi) eða ef þú ert að tengjast af einkaneti sem krefst þess að fara um milliþjón.">
<!ENTITY torSettings.configure "Stilla">
<!ENTITY torSettings.connect "Tengjast">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Bíð eftir að Tor ræsist...">
<!ENTITY torsettings.restartTor "Endurræsa Tor">
<!ENTITY torsettings.reconfigTor "Endurstilla">

<!ENTITY torsettings.discardSettings.prompt "Þú hefur sett upp brýr fyrir Tor eða að þú hefur farið inn í stillingar milliþjóns á kerfinu.&#160; Til að útbúa beina tengingu inn á Tor-netið, þá verður að fjarlægja þessar stillingar.">
<!ENTITY torsettings.discardSettings.proceed "Fjarlægja stillingar og tengjast">

<!ENTITY torsettings.optional "Valkvætt">

<!ENTITY torsettings.useProxy.checkbox "Ég nota milliþjón (proxy) til að tengjast við internetið">
<!ENTITY torsettings.useProxy.type "Tegund milliþjóns (proxy)">
<!ENTITY torsettings.useProxy.type.placeholder "veldu tegund milliþjóns">
<!ENTITY torsettings.useProxy.address "Vistfang">
<!ENTITY torsettings.useProxy.address.placeholder "IP-vistfang eða vélarheiti">
<!ENTITY torsettings.useProxy.port "Gátt">
<!ENTITY torsettings.useProxy.username "Notandanafn">
<!ENTITY torsettings.useProxy.password "Lykilorð">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Þessi tölva tengist í gegnum eldvegg sem leyfir einungis tengingar í gegnum tilteknar gáttir">
<!ENTITY torsettings.firewall.allowedPorts "Leyfðar gáttir">
<!ENTITY torsettings.useBridges.checkbox "Tor er ritskoðað í landinu mínu">
<!ENTITY torsettings.useBridges.default "Veldu innbyggða brú">
<!ENTITY torsettings.useBridges.default.placeholder "veldu brú">
<!ENTITY torsettings.useBridges.bridgeDB "Biðja um brú frá torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Settu inn stafina úr myndinni">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Fá nýja gátu">
<!ENTITY torsettings.useBridges.captchaSubmit "Senda inn">
<!ENTITY torsettings.useBridges.custom "Gefa brú sem ég þekki">
<!ENTITY torsettings.useBridges.label "Settu inn upplýsingar um brú frá aðila sem þú treystir.">
<!ENTITY torsettings.useBridges.placeholder "settu inn vistfang:gátt (eitt á hverja línu)">

<!ENTITY torsettings.copyLog "Afrita atvikaskrá Tor á klippispjald">

<!ENTITY torsettings.proxyHelpTitle "Hjálp fyrir milliþjóna">
<!ENTITY torsettings.proxyHelp1 "Hugsanlega þarf að nota staðværan milliþjón (proxy) þegar tengst er í gegnum net fyrirtækis, skóla eða stofnunar.&#160;Ef þú ert ekki viss hvort þurfi að nota milliþjón, kíktu þá á netstillingarnar í einhverjum öðrum vafra eða á uppsetningu netsins í stýrikerfinu þínu.">

<!ENTITY torsettings.bridgeHelpTitle "Hjálp fyrir brúaendurvarpa">
<!ENTITY torsettings.bridgeHelp1 "Brýr eru faldir endurvarpar sem gera erfiðara að loka á tengingar á Tor netinu.&#160; Hver tegund af brúm notar mismunandi aðferðir til að forðast ritskoðun.&#160; obfs-brýrnar láta netumferðina þína líta út sem handahófskennt suð og meek-brýr láta netumferðina þína líta út fyrir að tengjast þeirri þjónustu í staðinn fyrir Tor.">
<!ENTITY torsettings.bridgeHelp2 "Vegna þess hvernig sum lönd reyna að loka á umferð um Tor, munu sumar brýr virka í sumum löndum en aðrar ekki.&#160; Ef þú ert ekki viss um hvaða brýr virka í landinu þínu, skaltu skoða torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Bíddu aðeins á meðan tengingu er komið á við Tor-netið.&#160; Það getur tekið nokkrar mínútur.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Tenging">
<!ENTITY torPreferences.torSettings "Stillingar Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor-vafrinn beinir umferðinni þinni um Tor-netið, sem rekið er af þúsundum sjálfboðaliða um víða veröld." >
<!ENTITY torPreferences.learnMore "Fræðast frekar">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Nettengt">
<!ENTITY torPreferences.statusInternetOffline "Ónettengt">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Tengt">
<!ENTITY torPreferences.statusTorNotConnected "Ótengt">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Fræðast frekar">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Flýtiræsing">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Alltaf tengjast sjálfkrafa">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Brýr">
<!ENTITY torPreferences.bridgesDescription "Brýr hjálpa þér við að fá aðgang að Tor-netkerfinu á stöðum þar sem lokað er á Tor. Það fer eftir því hvar þú ert hvaða brú virkar betur en einhver önnur.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Sjálfvirkt">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Fjarlægja">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Afritað!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Biðja um brú…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Nánar">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Skoða atvikaskrár…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "Þessa aðgerð er ekki hægt að afturkalla.">
<!ENTITY torPreferences.cancel "Hætta við">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Biðja um brú">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Tengist BridgeDB. Bíddu aðeins.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Leystu CAPTCHA-þrautina til að biðja um brú.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Þessi lausn er ekki rétt. Reyndu aftur.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Settu inn upplýsingar um brúna sem þú fékkst frá áreiðanlegum aðila">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor atvikaskrá (log)">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Ótengt">
<!ENTITY torConnect.connectingConcise "Tengist…">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Sjálfvirkt">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Stilla tengingu…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Reyndu aftur">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor-vafranum mistókst að koma á tengingu við Tor-netið">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Prófa brú">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
