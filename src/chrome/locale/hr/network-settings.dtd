<!ENTITY torsettings.dialog.title "Postavke Tor mreže">
<!ENTITY torsettings.wizard.title.default "Spoji se na Tor">
<!ENTITY torsettings.wizard.title.configure "Postavke Tor mreže">
<!ENTITY torsettings.wizard.title.connecting "Uspostavljanje veze">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Jezik Tor preglednika">
<!ENTITY torlauncher.localePicker.prompt "Odaberi jezik">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Za povezivanje s Torom, pritisni „Poveži se”">
<!ENTITY torSettings.configurePrompt "Ako si u zemlji koja cenzurira Tor (kao Egipat, Kina, Turska) ili si spojen/a preko privatne mreže za koju je potreban poslužitelj, promijeni postavke mreže pritiskom na „Podesi”.">
<!ENTITY torSettings.configure "Podesi">
<!ENTITY torSettings.connect "Poveži se">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Čekanje na pokretanje Tora …">
<!ENTITY torsettings.restartTor "Ponovo pokreni Tor">
<!ENTITY torsettings.reconfigTor "Ponovo podesi">

<!ENTITY torsettings.discardSettings.prompt "Postavio/la si Tor mostove ili si upisao/la postavke lokalnog poslužitelja.&#160; Za ostvarivanje direktne veze s Tor mrežom, moraš ukloniti te postavke.">
<!ENTITY torsettings.discardSettings.proceed "Ukloni postavke i poveži se">

<!ENTITY torsettings.optional "Neobavezno">

<!ENTITY torsettings.useProxy.checkbox "Koristim poslužitelja za povezivanje na internet">
<!ENTITY torsettings.useProxy.type "Vrsta poslužitelja">
<!ENTITY torsettings.useProxy.type.placeholder "odaberi vrstu poslužitelja">
<!ENTITY torsettings.useProxy.address "Adresa">
<!ENTITY torsettings.useProxy.address.placeholder "IP adresa ili ime računala">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Korisničko ime">
<!ENTITY torsettings.useProxy.password "Lozinka">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Ovo računalo ide kroz vatrozid koji dozvoljava veze samo s određenim portovima">
<!ENTITY torsettings.firewall.allowedPorts "Dozvoljeni portovi">
<!ENTITY torsettings.useBridges.checkbox "Tor je cenzuriran u mojoj zemlji">
<!ENTITY torsettings.useBridges.default "Odaberi ugrađeni most">
<!ENTITY torsettings.useBridges.default.placeholder "odaberi most">
<!ENTITY torsettings.useBridges.bridgeDB "Zatraži most od torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Upiši znakove sa slike">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Nabavi novi izazov">
<!ENTITY torsettings.useBridges.captchaSubmit "Pošalji">
<!ENTITY torsettings.useBridges.custom "Pruži mi most koji poznam">
<!ENTITY torsettings.useBridges.label "Upiši podatke o mostu s pouzdanog izvora.">
<!ENTITY torsettings.useBridges.placeholder "upiši adresa:port (jednu po retku)">

<!ENTITY torsettings.copyLog "Kopiraj Tor zapisnik u međuspremnik">

<!ENTITY torsettings.proxyHelpTitle "Pomoć poslužitelja">
<!ENTITY torsettings.proxyHelp1 "Ako se povezuješ kroz mrežu tvrtke, škole ili sveučilišta moguće je da ćeš trebati lokalni poslužitelj.&#160;Ako ne znaš točno je li ti poslužitelj treba, pregledaj postavke interneta u jednom drugom pregledniku ili provjeri postavke mreže tvog sustava.">

<!ENTITY torsettings.bridgeHelpTitle "Pomoć s mostovima">
<!ENTITY torsettings.bridgeHelp1 "Mostovi su nenavedeni releji koji otežavaju blokiranje veza s Tor mrežom.&#160; Svaka vrsta mosta koristi drugačiju metodu kako bi se izbjegla cenzura.&#160; Obfs metode čine da tvoj promet izgleda kao slučajni šum, a meek metode čine da tvoj promet izgleda kao da se povezuje na tu uslugu, umjesto na Tor.">
<!ENTITY torsettings.bridgeHelp2 "Budući da neke zemlje pokušavaju blokirati Tor, određeni mostovi djeluju u određenim zemljama, ali ne i u drugima.&#160;  Ako ne znaš točno koji mostovi rade u tvojoj zemlji, posjeti torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Pričekaj dok uspostavljamo vezu s Tor mrežom. &#160; Ovo može potrajati nekoliko minuta.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Veza">
<!ENTITY torPreferences.torSettings "Tor postavke">
<!ENTITY torPreferences.torSettingsDescription "Tor preglednik usmjerava tvoj promet na Tor mrežu, koju pokreću tisuće volontera širom svijeta." >
<!ENTITY torPreferences.learnMore "Saznaj više">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Izvanmrežno">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Spojeno">
<!ENTITY torPreferences.statusTorNotConnected "Nepovezano">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Saznaj više">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Brzo pokretanje">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Uvijek poveži automatski">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Mostovi">
<!ENTITY torPreferences.bridgesDescription "Mostovi pomažu pristupiti Tor-mreži na mjestima gdje je Tor blokiran. Ovisno o tome gdje se nalaziš, neki mostovi rade bolje od drugih.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatski">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Ukloni">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Kopirano!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Zatraži most …">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Napredno">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Pogledaj log-zapise …">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "Ova radnja ne može biti opozvana.">
<!ENTITY torPreferences.cancel "Odustani">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Zatraži most">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Kontaktiranje baze podataka BridgeDB. Pričekaj.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Riješi CAPTCHA za traženje mosta.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Rješenje nije ispravno. Pokušaj ponovo.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Upiši podatke mosta od pouzdanog izvora">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Log-zapisi Tora">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Nepovezano">
<!ENTITY torConnect.connectingConcise "Spajanje …">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatski">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Pokušaj ponovno">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor preglednik nije uspio uspostaviti vezu s Tor mrežom">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
