<!ENTITY torsettings.dialog.title "Tor ကွန်ယက် အပြင်အဆင်များ">
<!ENTITY torsettings.wizard.title.default "Tor နှင့် ချိတ်ဆက်မယ်">
<!ENTITY torsettings.wizard.title.configure "Tor ကွန်ယက် အပြင်အဆင်များ">
<!ENTITY torsettings.wizard.title.connecting "ချိတ်ဆက်မှုလိုင်း တည်ထောင်နေသည်">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor ဘရောင်ဇာ ဘာသာစကား">
<!ENTITY torlauncher.localePicker.prompt "ကျေးဇူးပြု၍ ဘာသာစကားရွေးချယ်ပါ">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Tor နှင့် ချိတ်ဆက်ရန် &quot;ချိတ်ဆက်မယ်&quot; ဆိုသည့် ခလုတ်အား နှိပ်ပါ">
<!ENTITY torSettings.configurePrompt "ကွန်ယက် အပြင်အဆင်များ ချိန်ညှိရန် &quot;စီစဥ်မယ်&quot; ဆိုသည့်ခလုတ်အား နှိပ်ပါ">
<!ENTITY torSettings.configure "စီစဥ်မယ်">
<!ENTITY torSettings.connect "ချိတ်ဆက်မယ်">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Tor စဖို့ စောင့်နေပါသည်...">
<!ENTITY torsettings.restartTor "Tor ကို ပြန်လည်စတင်မယ်">
<!ENTITY torsettings.reconfigTor "ပြန်ချိန်ညှိမယ်">

<!ENTITY torsettings.discardSettings.prompt "Tor ချိတ်ဆက်တံတားများအား သင်စီမံထားရှိပြီးပါပြီ သို့မဟုတ် လိုကယ်ကြားခံ proxy များ၏ အပြင်အဆင်များကို ရိုက်ထည့်ပြီးပါပြီ။ ​&#160; Tor ကွန်ယက်နှင့် တိုက်ရိုက်ချိတ်ဆက်ရန် ၎င်းအပြင်အဆင်များကို ဖယ်ရှားရပါမည်။">
<!ENTITY torsettings.discardSettings.proceed "အပြင်အဆင်များ ဖယ်ရှားပြီး ချိတ်ဆက်မယ်">

<!ENTITY torsettings.optional "မဖြစ်မနေမဟုတ်">

<!ENTITY torsettings.useProxy.checkbox "ကျွန်ုပ်သည် ကြားခံ proxy သုံး၍ အင်တာနက်နှင့် ချိတ်ဆက်ပါသည်">
<!ENTITY torsettings.useProxy.type "ကြားခံ proxy အမျိုးအစား">
<!ENTITY torsettings.useProxy.type.placeholder "ကြားခံ proxy အမျိုးအစားတစ်ခု ရွေးပါ">
<!ENTITY torsettings.useProxy.address "လိပ်စာ">
<!ENTITY torsettings.useProxy.address.placeholder "IP လိပ်စာ သို့မဟုတ် ဧည့်ခံသူအမည် (hostname)">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "အသုံးပြုသူအမည်">
<!ENTITY torsettings.useProxy.password "စကားဝှက်">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "အချို့သော port များနှင့်သာ ချိတ်ဆက်မှုများခွင့်ပြုသည့် အသွားအလာထိန်းချုပ်သည့်အလွှာ (firewall) အား ဤကွန်ပျူတာသည် ဖြတ်သန်းပါသည်။">
<!ENTITY torsettings.firewall.allowedPorts "ခွင့်ပြုထားသော ports">
<!ENTITY torsettings.useBridges.checkbox "ကျွန်ုပ်၏ နိုင်ငံတွင် Tor ကို ဆင်ဆာခံထားပါသည်">
<!ENTITY torsettings.useBridges.default "ပင်ကိုသွင်းထားသော ချိတ်ဆက်တံတား ရွေးပါ">
<!ENTITY torsettings.useBridges.default.placeholder "ချိတ်ဆက်တံတား ရွေးပါ">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org မှ ချိတ်ဆက်တံတား တောင်းဆိုပါ">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "ရုပ်ပုံထဲမှ စာလုံးများကို ရိုက်ထည့်ပါ">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "စိန်ခေါ်မှုအသစ် ယူမယ်">
<!ENTITY torsettings.useBridges.captchaSubmit "တင်သွင်းပါ။">
<!ENTITY torsettings.useBridges.custom "ကျွန်ုပ်သိသော ချိတ်ဆက်တံတား ပံ့ပိုးပါ">
<!ENTITY torsettings.useBridges.label "ယုံကြည်ရသော အရင်းအမြစ်မှ ချိတ်ဆက်တံတား အချက်အလက် ရိုက်ထည့်ပါ">
<!ENTITY torsettings.useBridges.placeholder "လိပ်စာအမျိုးအစား : port (တစ်ကြောင်းတစ်ခုစီ)">

<!ENTITY torsettings.copyLog "Tor မှတ်တမ်းကို စာဘုတ်ပေါ်သို့ ကော်ပီကူးယူမယ်">

<!ENTITY torsettings.proxyHelpTitle "Proxy ကြားခံ အကူအညီ">
<!ENTITY torsettings.proxyHelp1 "ကုမ္ပဏီ၊ ကျောင်း၊ သို့မဟုတ် တက္ကသိုလ် ကွန်ယက်များနှင့် ချိတ်ဆက်လျှင် လိုကယ် ကြားခံ proxy လိုအပ်နိုင်ပါသည်။ &#160; သင်မှ ကြားခံ proxy လိုမလိုခြင်း မသေချာပါက အခြား ဘရောင်ဇာမှ အင်တာနက် အပြင်အဆင်များ သို့မဟုတ် သင့်စနစ်၏ ကွန်ယက်ချိတ်ဆက်မှု အပြင်အဆင်များကို လေ့လာကြည့်ရှုနိုင်ပါသည်။">

<!ENTITY torsettings.bridgeHelpTitle "ချိတ်ဆက်တံတား လက်ဆင့်ကမ်းခြင်း အကူအညီ">
<!ENTITY torsettings.bridgeHelp1 "ချိန်ဆက်တံတားများ သည် Tor ကွန်ယက်ချိတ်ဆက်မှုများ ကို ပိတ်ပယ်ရန် ခက်ခဲအောင်လုပ်သည့် စာရင်းမသွင်းထားသော လက်ဆင့်ကမ်းမှုများ ဖြစ်ပါသည်။ .&#160; ဆင်ဆာခံရခြင်းကို ရှောင်ကျင်ရန် ချိတ်ဆက်တံတား အမျိုးအစားများတစ်ခုစီသည် ကွဲပြားသော နည်းလမ်းများ အသုံးပြုပါသည်။ .&#160; Obfs အမျိုးအစားသည် သင့် အင်တာနက် အသွားအလာများကို ကျပန်းဆူညံသံကဲ့သို့ အယောင်ဆောင်ပြုပါသည်၊ Meeks အမျိုးအစားသည် သင့် အင်တာနက် အသွားအလာများ သို့မဟုတ် Tor နှင့် ချိတ်ဆက်ထားခြင်းကို အခြားဝန်ဆောင်မှုနှင့် ချိတ်ဆက်ထားခြင်းကဲ့သို့ အယောင်ဆောင်ပြုပါသည်။">
<!ENTITY torsettings.bridgeHelp2 "အချို့သောနိုင်ငံများတွင် Tor ကို ပိတ်ပယ်ရန် ကြိုးစားခြင်းကြောင့် အချို့သော ချိတ်ဆက်တံတားများသည် အချို့နိုင်ငံတွင်သာ အလုပ်လုပ်ပြီး အခြားနိုင်ငံများတွင် အလုပ်မလုပ်ပါ။ ​&#160; သင့်နိုင်ငံတွင် မည်သည့် ချိတ်ဆက်တံတားများက အလုပ်လုပ်သည်ကို မသေချာလျှင် torproject.org/about/contact.html သို့ ဝင်ရောက်ကြည့်ရှုပေးပါ။ #support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "ကျွန်ုပ်တို့မှ​ Tor ကွန်ယက်နှင့် ချိတ်ဆက်မှု ထူထောင်နေတုန်း ခေတ္တစောင့်ပေးပါ။ &#160; ဤလုပ်ဆောင်မှုသည် မိနစ်အနည်းအငယ်ကြာနိုင်လိမ့်မည်။">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "ချိတ်ဆက်ခြင်း">
<!ENTITY torPreferences.torSettings "Tor အပြင်အဆင်များ">
<!ENTITY torPreferences.torSettingsDescription "ကမ္ဘာအနှံ့ရှိ လုပ်အားပေးများ လုပ်ဆောင်သော Tor ကွန်ယက်ပေါ်တွင် သင့်အသွားအလာများကို Tor ဘရောင်ဇာမှ လမ်းကြောင်းပေးပါသည်။" >
<!ENTITY torPreferences.learnMore "ထပ်မံလေ့လာမည်">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "စမ်းသပ်ရန်">
<!ENTITY torPreferences.statusInternetOnline "လိုင်းတက်နေသည်">
<!ENTITY torPreferences.statusInternetOffline "အော့ဖ်လိုင်း">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "ချိတ်ဆက်ပြီးပါပြီ">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "ထပ်မံလေ့လာမည်">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Bridges">
<!ENTITY torPreferences.bridgesDescription "ချိတ်ဆက်တံတားများသည် Tor ကို ပိတ်ပယ်ထားသော နေရာများတွင် Tor ကွန်ယက်နှင့် ချိတ်ဆက်ရန် ကူညီပေးပါသည်။ သင့် တည်နေရာပေါ်မူတည်၍ ချိတ်ဆက်တံတားတစ်ခုသည် နောက်တစ်ခုထက် ပိုကောင်းနိုင်ပါလိမ့်မည်။">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatic">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "ဖယ်ရှားမည်">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "ကော်ပီကူးပြီး!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "ချိတ်ဆက်တံတား တောင်းဆိုပါ...">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "အဆင့်မြင့်">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "မှတ်တမ်းများ ကြည့်ရှုမယ်...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "ပယ်ဖျက်မည်">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "ချိတ်ဆက်တံတား တောင်းဆိုမယ်">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "BridgeDB နှင့် ဆက်သွယ်နေသည်။ ခေတ္တစောင့်ပေးပါ။">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "လူဟုတ်မဟုတ်စစ်ဆေးမှုကို ဖြေ၍ bridge အား တောင်းဆိုပါ">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "ဖြေရှင်းမှုသည် မမှန်ကန်ပါ။ ပြန်စမ်းကြည့်ပေးပါ။">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor မှတ်တမ်းများ">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "ချိတ်ဆက်နေသည်…">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatic">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "ထပ်မံလုပ်ဆောင်ပါ။">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
